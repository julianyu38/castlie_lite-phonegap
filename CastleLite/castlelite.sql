/*
SQLyog Ultimate v8.55 
MySQL - 5.1.37 : Database - castle_lite_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`castle_lite_db` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;

USE `castle_lite_db`;

/*Table structure for table `board` */

DROP TABLE IF EXISTS `board`;

CREATE TABLE `board` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `song_name` varchar(250) COLLATE utf8_bin NOT NULL,
  `upload_file` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `vote_sum` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=389 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `board` */

insert  into `board`(`id`,`song_name`,`upload_file`,`created`,`modified`,`vote_sum`) values (388,'audio_001','12012120518185993396604900179490.mp3','2012-12-05 18:18:59','0000-00-00 00:00:00',NULL),(387,'rec_001','1201212051304025403452607104940229250mp3','2012-12-05 13:04:02','0000-00-00 00:00:00',NULL);

/*Table structure for table `ci_sessions` */

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `ci_sessions` */

/*Table structure for table `login_attempts` */

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `login_attempts` */

/*Table structure for table `user_autologin` */

DROP TABLE IF EXISTS `user_autologin`;

CREATE TABLE `user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `user_autologin` */

/*Table structure for table `user_profiles` */

DROP TABLE IF EXISTS `user_profiles`;

CREATE TABLE `user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `user_profiles` */

insert  into `user_profiles`(`id`,`user_id`,`country`,`website`) values (1,1,NULL,NULL),(2,2,NULL,NULL),(3,5,NULL,NULL),(4,6,NULL,NULL),(5,7,NULL,NULL),(6,8,NULL,NULL),(7,9,NULL,NULL),(8,10,NULL,NULL),(9,11,NULL,NULL),(10,12,NULL,NULL),(11,13,NULL,NULL),(12,14,NULL,NULL),(13,15,NULL,NULL),(14,16,NULL,NULL),(15,17,NULL,NULL),(16,18,NULL,NULL),(17,19,NULL,NULL),(18,20,NULL,NULL),(19,21,NULL,NULL),(20,22,NULL,NULL),(21,23,NULL,NULL),(22,24,NULL,NULL),(23,25,NULL,NULL),(24,26,NULL,NULL),(25,27,NULL,NULL),(26,28,NULL,NULL),(27,29,NULL,NULL),(28,30,NULL,NULL),(29,31,NULL,NULL),(30,32,NULL,NULL),(31,33,NULL,NULL),(32,34,NULL,NULL),(33,35,NULL,NULL),(34,36,NULL,NULL),(35,37,NULL,NULL),(36,38,NULL,NULL),(37,39,NULL,NULL),(38,40,NULL,NULL),(39,41,NULL,NULL),(40,42,NULL,NULL),(41,43,NULL,NULL),(42,44,NULL,NULL),(43,45,NULL,NULL),(44,46,NULL,NULL),(45,47,NULL,NULL),(46,48,NULL,NULL),(47,49,NULL,NULL),(48,50,NULL,NULL),(49,51,NULL,NULL),(50,52,NULL,NULL),(51,53,NULL,NULL),(52,54,NULL,NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `role_id` tinyint(4) NOT NULL DEFAULT '0',
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`password`,`role_id`,`email`,`activated`,`banned`,`ban_reason`,`new_password_key`,`new_password_requested`,`new_email`,`new_email_key`,`last_ip`,`last_login`,`created`,`modified`) values (1,'admin','$2a$08$SpB2BuRqcdmZPz50vrVEeeZTaZV4jfCpO40d/TL6IbVqwBZttBeui',1,'admin@admin.com',1,0,NULL,NULL,NULL,'admin@hotmaill.com','b64ac11cd31e89c86af0192cef348d7a','175.160.104.102','2012-11-29 10:15:05','2012-11-14 17:06:06','2012-11-29 10:14:41'),(2,'koala','$2a$08$KiuKZcczIkd4yCmQl9F43OksAX9X4FPM9.T.iHoOXNJvjTd9pa/aq',0,'koala@hotmail.com',1,0,NULL,NULL,NULL,'koala216@hotmail.com','d5a1021ccf4e036ed97971553017aee0','127.0.0.1','2012-11-18 11:38:57','2012-11-17 14:23:31','2012-11-18 12:14:37'),(30,'','$2a$08$VyI63qNwyEILOx1xfFpPTum9A6ohKnpkezMa52kpeGcgFlH7D7h7a',0,'',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.103.10','0000-00-00 00:00:00','2012-11-21 09:21:02','2012-11-21 09:20:38'),(32,'aaa','$2a$08$vNvDxIU8DHXA5Eaa11QdZeW7nJ08kRVJ0J2j0TmXuU58A3.5iK52G',0,'aaa@aaa.com',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.103.1','0000-00-00 00:00:00','2012-11-21 10:55:22','2012-11-21 10:54:58'),(33,'111','$2a$08$DXOjp85djEWTeYyg2jDsu.524z6Upa4gBAxsnTn14GbqYqv0J0Iqi',0,'111@111.com',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.103.10','2012-11-21 10:57:22','2012-11-21 10:57:07','2012-11-21 10:56:58'),(34,'Bbb','$2a$08$X0f7o2hfFJMH9I0JsSgGu.OHiJxvsftvu3EVbgl5h0fY0f6Mro0ua',0,'Bbb@BBC.com',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.107.202','2012-11-21 11:06:45','2012-11-21 10:58:07','2012-11-21 11:06:21'),(35,'ccc','$2a$08$zlhzE6Z8Sqhlo8eftpDfF.wTiL4wyadgNCLGOdJkxrRG7DRpDsF1y',0,'ccc@ccc.com',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.103.10','0000-00-00 00:00:00','2012-11-21 11:00:39','2012-11-21 11:00:15'),(36,'FCC','$2a$08$IwDYVoz5rhzZOiif1oQDJuf/i1yCLZAStfII.4GJG/RC30/B68Suy',0,'Ccc@kcc.com',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.107.202','0000-00-00 00:00:00','2012-11-21 11:01:29','2012-11-21 11:01:05'),(37,'abc','$2a$08$OWCyLxkdLlQvFShP9nqsgedfowDVZsMc0Tj5IfpIZjT/iSa6Z8snG',0,'abc@abc.com',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.103.1','0000-00-00 00:00:00','2012-11-21 11:02:51','2012-11-21 11:02:27'),(38,'Gggggfee','$2a$08$n4hI0CF/WAFXXuCW7MjEWeIOV7YeNbMFUDs1fYvmdDDgV5tnV.bT2',0,'Gggg@hhh.com',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.107.202','0000-00-00 00:00:00','2012-11-21 11:11:47','2012-11-21 11:11:23'),(39,'1','$2a$08$yQpKp25GKMw7IhiqlbapWu2Lo5lBy1ztgay8NUxpBUEEg6VG0ym9S',0,'1',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.1.105','2012-12-05 18:18:07','2012-11-21 11:12:36','2012-12-05 18:17:43'),(40,'3','$2a$08$rJBYf7SW9JdFoUUQLuz9buLGqnJlS0BkddmxoU0w0kdzsFShn6kKq',0,'3',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.103.10','0000-00-00 00:00:00','2012-11-21 11:13:13','2012-11-21 11:12:49'),(41,'Ghftvb','$2a$08$5bYmIq6arKnzf4pgf/HGge5YlcOZCTK2Qy/fT/NEwpm/8DWNRzxSu',0,'Jkhy@gfh.com',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.107.202','0000-00-00 00:00:00','2012-11-21 11:13:45','2012-11-21 11:13:21'),(42,'211212','$2a$08$dEU9ZbE5g0EYEKQ0CuAwy.1ydwUB8dLxxUU8c.TvK6E6/45KSfhMK',0,'211212@211221.com',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.103.10','0000-00-00 00:00:00','2012-11-21 11:17:12','2012-11-21 11:16:48'),(43,'2','$2a$08$gOeVmRW8j/Ow6hze5jxZ5.xNPxPnJcj7HIU4u8qgYqF4QCBu4OVPG',0,'2',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.103.10','0000-00-00 00:00:00','2012-11-23 01:00:06','2012-11-23 00:59:42'),(44,'121212','$2a$08$WigwpNJeFgrliPieQudgueY1ii92JJLbcWtvsh8PhRGjg6BjVPRI2',0,'12121',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.103.10','0000-00-00 00:00:00','2012-11-23 01:00:18','2012-11-23 00:59:54'),(45,'34443434','$2a$08$4wXIhRm/gAkvzPruzLJBGO/7jQyNOjyhYeUB8aPjenhx3R4ln9Akm',0,'3433434434',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.104.118','0000-00-00 00:00:00','2012-11-23 01:00:39','2012-11-23 01:00:15'),(46,'ds','$2a$08$6IBnHlwEnDxrY3ghrxGM8ug4Ii6Zl3Yru2AAjuX06oTzqF8peWPRu',0,'ds',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.103.10','0000-00-00 00:00:00','2012-11-23 05:38:18','2012-11-23 05:37:54'),(47,'5','$2a$08$n5ZjqmA9ZpPcW837MZRieOtDV5mfLNXDaqUSoefdn9xQiQNWY.Tuq',0,'5',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.104.108','0000-00-00 00:00:00','2012-11-23 09:09:00','2012-11-23 09:08:36'),(48,'511','$2a$08$ImDg6THLOE4ZKcvY1ap4je.jUm.BInmoJ8L9TdzMMBxqXE7lYQEJm',0,'5111',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.104.119','0000-00-00 00:00:00','2012-11-26 02:08:07','2012-11-26 02:07:43'),(49,'123','$2a$08$0bZeh/8e0w.ImHPwB2uhZeLx9NUK.V3aQpKCaowgSt/PWQGzPYuUu',0,'123',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.104.119','0000-00-00 00:00:00','2012-11-26 02:42:16','2012-11-26 02:41:52'),(50,'1111111111','$2a$08$i/gPfu3n8Iay/jxjGIzxieTLpDqWc8UZs76g5LetHpm2sjWZAxAEe',0,'1111111',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.104.103','0000-00-00 00:00:00','2012-11-27 03:55:06','2012-11-27 03:54:42'),(51,'008','$2a$08$StlA2xA/xzj0u6q67/SwKO31dK6BYxJVt05y2Hny/c3giElmRYfxG',0,'008',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.103.10','0000-00-00 00:00:00','2012-11-29 06:19:29','2012-11-29 06:19:05'),(52,'Fewfewfewffew','$2a$08$sS1w2xHDsYKciOi090HnT./YZSaXo8DAoLZOPii6PGinvbCkEy5D2',0,'fewfewfewfwefFewfewfewffewFewfewfewfwef',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.104.102','0000-00-00 00:00:00','2012-11-29 08:23:14','2012-11-29 08:22:50'),(53,'4325432','$2a$08$22IKr5vSiYdrlLwB.HHjM.KHsPwrpMKfmG/quTEMdNMzr9bmtvwlO',0,'5432',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.104.102','0000-00-00 00:00:00','2012-11-29 14:38:18','2012-11-29 14:37:54'),(54,'fdsafdsafdsaf','$2a$08$mNrRGqVsFX3GF4dWYmzNWu5YeRV9XtkTC.bNGu.wOw4fcfmbYw9ee',0,'fdsafdsaf',1,0,NULL,NULL,NULL,NULL,NULL,'175.160.104.102','0000-00-00 00:00:00','2012-11-29 14:43:34','2012-11-29 14:43:10');

/*Table structure for table `vote_tbl` */

DROP TABLE IF EXISTS `vote_tbl`;

CREATE TABLE `vote_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) DEFAULT NULL,
  `songid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=437 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `vote_tbl` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
