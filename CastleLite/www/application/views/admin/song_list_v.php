<script type="text/javascript">
<!--
	function song_add() {
		window.location.href = "<?php echo site_url("admin/user/".$post_key."_add"); ?>";
		return false;
	}
	function confirm_del(sid) {
		if(!confirm('Are you sure to delete?')) {
			return;
		}
		window.location.href = "<?php echo site_url("admin/user/".$post_key."_del"); ?>/" + sid;
	}
	function goedit(sid) {
		window.location.href = "<?php echo site_url("admin/user/".$post_key."_edit"); ?>/" + sid;
	}
//-->
</script>
<article class="module width_full">
<header>
	<h3 class="tabs_involved">Leader Board</h3>
	<div class="submit_link">
		<!-- <input type="submit" value="Add" class="alt_btn" onclick="return song_add()" /> -->
	</div>
</header>
<div class="tab_container">
	<table class="tablesorter" cellspacing="0">
		<thead>
			<tr>
				<th width="30">No.</th>
				<th width="120">Song name</th>
				<th >File info</th>
				<th width="100" >Vote</th>
				<th width="80">Actions</th>
			</tr>
		</thead>
		<tbody>
<?php
		$i = 0;
		foreach($rows as $song) {
			$link = site_url("admin/user/".$post_key."_edit/".$song['id']);			
?>
		<tr>
			<td><?php echo $song['id'];?></td>
			<td><a href="javascript:goedit(<?php echo $song['id']; ?>)"><?php echo $song['song_name'];?></a></td>
			<td><?php echo $song['upload_file'];?></td>
			<td><?php echo $song['vote_sum'];?></td>
			<td>
				<input type="image" title="Edit" src="<?php echo IMG_DIR; ?>/icn_edit.png" onclick="goedit(<?php echo $song['id'];?>)">
				<input type="image" title="Trash" src="<?php echo IMG_DIR; ?>/icn_trash.png" onclick="confirm_del(<?php echo $song['id'];?>)">
			</td>
		</tr>
<?php
			$i++;
		}
		if($i==0) {
			echo "<tr><td colspan='4' align='center'>Nothing </td></tr>";
		}
?>
		</tbody>
	</table>
	</div>
</article>