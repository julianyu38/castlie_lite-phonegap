<article class="module width_full">
<header>
	<h3> User	Edit ( <?php echo $post['id']; ?> ) </h3>
</header>
<?php
$username = array(
	'name'	=> 'username',
	'id'	=> 'username',
	'value' => $post['username'],
	'maxlength'	=> $this->config->item('username_max_length', 'tank_auth'),
	'size'	=> 30,	
	'readonly' => 'readonly',	
);
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'value'	=> $post['email'],
	'maxlength'	=> 80,
	'size'	=> 30,
);
$old_password = array(
	'name'	=> 'old_password',
	'id'	=> 'old_password',
	'value' => set_value('old_password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
);
$new_password = array(
	'name'	=> 'new_password',
	'id'	=> 'new_password',
	'value' => set_value('new_password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
);
$confirm_new_password = array(
	'name'	=> 'confirm_new_password',
	'id'	=> 'confirm_new_password',
	'value' => set_value('confirm_new_password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
);
	
	echo form_open_multipart($this->uri->uri_string());
	
	if(!empty($show_message)) {
		echo "<h4 class='alert_success'>".$show_message."</h4>";
	}else{
		$this->form_validation->set_error_delimiters('<h4 class="alert_error">', '</h4>');
		echo form_error($username['name']);
		if (isset($show_errors)) {?>
			<h4 class="alert_error">
				<?php if (is_array($show_errors)) {?>
				<?php foreach ($show_errors as $error) :?>
					<label><?=$error?></label>
				<?php endforeach;?>
				<?php } else {?>
					<label><?=$show_errors?></label>
				<?php } ?>
			</h4>
<?php 
		}
	}
?>
<div class="module_content">
<fieldset>
<table>
	<tr>
		<td><?php echo form_label('Username', $username['id']); ?></td>
		<td><?php echo form_input($username); ?></td>
		<td style="color: red;"><?php echo form_error($username['name']); ?><?php echo isset($errors[$username['name']])?$errors[$username['name']]:''; ?></td>
	</tr>
	<tr>
		<td><?php echo form_label('Email Address', $email['id']); ?></td>
		<td><?php echo form_input($email); ?></td>
		<td style="color: red;"><?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?></td>
	</tr>
	<tr>
		<td><?php echo form_label('Old Password', $old_password['id']); ?></td>
		<td><?php echo form_password($old_password); ?></td>
		<td style="color: red;"><?php echo form_error($old_password['name']); ?></td>
	</tr>
	<tr>
		<td><?php echo form_label('New Password', $new_password['id']); ?></td>
		<td><?php echo form_password($new_password); ?></td>
		<td style="color: red;"><?php echo form_error($new_password['name']); ?></td>
	</tr>
	<tr>
		<td><?php echo form_label('Confirm Password', $confirm_new_password['id']); ?></td>
		<td><?php echo form_password($confirm_new_password); ?></td>
		<td style="color: red;"><?php echo form_error($confirm_new_password['name']); ?></td>
	</tr>	
</table>
</fieldset>
</div>

<footer>
<div class="submit_link">
	<input type="submit" value="Update" class="alt_btn">
</div>
</footer>

<?php echo form_close(); ?>

</article>