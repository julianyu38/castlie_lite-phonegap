<script type="text/javascript">
<!--
	function user_add() {
		window.location.href = "<?php echo site_url("admin/user/".$post_key."_add"); ?>";
		return false;
	}
	function confirm_del(uid) {
		if(!confirm('Are you sure to delete?')) {
			return;
		}
		window.location.href = "<?php echo site_url("admin/user/".$post_key."_del"); ?>/" + uid;
	}
	function goedit(uid) {
		window.location.href = "<?php echo site_url("admin/user/".$post_key."_edit"); ?>/" + uid;
	}
//-->
</script>
<article class="module width_full">
<header>
	<h3 class="tabs_involved">User List</h3>
	<div class="submit_link">
		<input type="submit" value="Add" class="alt_btn" onclick="return user_add()" />
	</div>
</header>
<div class="tab_container">
	<table class="tablesorter" cellspacing="0">
		<thead>
			<tr>
				<th width="30">No.</th>
				<th width="120">User name</th>
				<th >Email</th>
				<th width="80">Actions</th>
			</tr>
		</thead>
		<tbody>
<?php
		$i = 0;
		foreach($rows as $user) {
			$link = site_url("admin/user/".$post_key."_edit/".$user['id']);
			$banned = $user['banned'];
?>
		<tr>
			<td><?php echo $user['id'];?></td>
			<td><a href="javascript:goedit(<?php echo $user['id']; ?>)"><?php echo $user['username'];?></a></td>
			<td><?php echo $user['email'];?></td>
			<td>
				<input type="image" title="Edit" src="<?php echo IMG_DIR; ?>/icn_edit.png" onclick="goedit(<?php echo $user['id'];?>)">
				<input type="image" title="Trash" src="<?php echo IMG_DIR; ?>/icn_trash.png" onclick="confirm_del(<?php echo $user['id'];?>)">
			</td>
		</tr>
<?php
			$i++;
		}
		if($i==0) {
			echo "<tr><td colspan='4' align='center'>Nothing </td></tr>";
		}
?>
		</tbody>
	</table>
	</div>
</article>