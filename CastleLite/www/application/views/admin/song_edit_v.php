<article class="module width_full">
<header>
	<h3> Song	Edit ( <?php echo $post['id']; ?> ) </h3>
</header>
<?php
$songname = array(
	'name'	=> 'song_name',
	'id'	=> 'song_name',
	'value' => $post['song_name'],
	'size'	=> 30,		
);
	
	echo form_open_multipart($this->uri->uri_string());
	
	if(!empty($show_message)) {
		echo "<h4 class='alert_success'>".$show_message."</h4>";
	}else{
		$this->form_validation->set_error_delimiters('<h4 class="alert_error">', '</h4>');
		
		echo form_error($songname['name']);
		if (isset($show_errors)) {?>
			<h4 class="alert_error">
				<?php if (is_array($show_errors)) {?>
				<?php foreach ($show_errors as $error) :?>
					<label><?=$error?></label>
				<?php endforeach;?>
				<?php } else {?>
					<label><?=$show_errors?></label>
				<?php } ?>
			</h4>
<?php 
		}
	}
?>
<div class="module_content">
<fieldset>
<table>
	<tr>
		<td><?php echo form_label('Song Name', $songname['id']); ?></td>
		<td><?php echo form_input($songname); ?></td>
		<td style="color: red;"><?php echo form_error($songname['name']); ?><?php echo isset($errors[$songname['name']])?$errors[$songname['name']]:''; ?></td>
	</tr>
</table>
</fieldset>
</div>

<footer>
<div class="submit_link">
	<input type="submit" value="Update" class="alt_btn">
</div>
</footer>

<?php echo form_close(); ?>

</article>