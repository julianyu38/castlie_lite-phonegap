<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller
{
		
	function __construct()
	{
		session_start();
		
		parent::__construct();

		$this->load->database();
		//$this->load->library('security');
		$this->load->library('tank_auth');
		$this->load->model('admin_m');
		$this->load->model('sounds_m');
		//$this->load->model('audio_streamer.php');
		//$this->lang->load('tank_auth');		
	}
	
	function _remap($method) {
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json");
		$this->{$method}();
	}
	
	function index() {
		
	}
	
	function login() {
		
		$username 	= empty($_POST['uname'])? "":$_POST['uname'];
		$userpass 	= empty($_POST['upass'])? "":$_POST['upass'];
		
		if(empty($username) || empty($userpass)) {
			echo json_encode("Invalid");
			//return;
		}
		
		if ($username != '' || $userpass != '') {
			if ($this->tank_auth->login($username, $userpass, FALSE, TRUE, FALSE)) {								// success
				echo json_encode("LoginOk");
			} else {
				echo json_encode("LoginERR");
			}
		}
	}
	
	function logout() {
		
		$this->tank_auth->logout();

		echo json_encode("Logout");
	}
	
	
	function register() {
		
		$use_username = $_POST['uname'];
		$email = $_POST['umail'];
		$password = $_POST['upass'];
		
		$email_activation = $this->config->item('email_activation', 'tank_auth');

		if (!is_null($data = $this->tank_auth->create_user(	$use_username,
															$email,
															$password,
															$email_activation))) {	
			echo json_encode("RegOK");
		}else {
			echo json_encode("RegErr");
		}
	}
	
	function getBoardData() {
		
		$rows = $this->sounds_m->get_song_list();
				
		echo json_encode($rows);
		
	}
	
	// Record Sound Effect Merge
	
	function rec_merge() {
				
		$this->load->library('upload');
		
		if (isset($_FILES['file']) && $_FILES['file'] != '') {
			
			$file_name 	= str_replace(' ', '_', strtolower($_FILES['file']['name']));//upload file's name
			$filename 	= $_POST['fname'];//upload file's only filename
			$fileext 	= $_POST['fext'];//upload file's file ext
			$nowstamp 	= date("YmdGis");//now time
			$uname 		= $this->tank_auth->get_username();//username
			$temp_file 	= $filename.$file_name;//temp file
			$effect_file = $filename.".mp3";//effected file
			
			$conf = array();
			$conf['upload_path'] 	= $this->admin_m->get_upload_path(3,"temp");
			$conf['file_name']		= $file_name;
			if (!file_exists($conf['upload_path'])) {				
				mkdir($conf['upload_path']);				
			}			
			$this->upload->initialize($conf);
					
			if ($this->upload->do_upload('file', $temp_file)) {				
				
				$fileinfo = $this->upload->data();				
							
				//$this->sounds_m->merge_sounds($uname, $nowstamp, $_POST['mix1'], $_POST['mix2'], $_POST['mix3'], $_POST['mix4'], $_POST['mix5'], $_POST['female1'], $_POST['female2'], $_POST['female3'], $_POST['male1'], $_POST['male2'], $_POST['male3'], $_POST['fx1'], $_POST['fx2'], $_POST['fx3']);
				
				$mixer_count = 0;
				$mix_str = "";			
				//------- Sound Five Scroll Effect Action -------//
				
				$mix1 = $_POST['mix1']/100;
				$mix2 = $_POST['mix2']/100;
				$mix3 = $_POST['mix3']/100;
				$mix4 = $_POST['mix4']/100;
				$mix5 = $_POST['mix5']/100;
	
				if ( $mix1 > 0 ) {
					$mixer_count++;
					exec("sox --volume ".$mix1." backsounds/Main001.mp3 temp/".$filename."mixmain1.mp3");
					$mix_str .= " -i temp/".$filename."mixmain1.mp3";
				}
				if ( $mix2 > 0 ) {
					$mixer_count++;
					exec("sox --volume ".$mix2." backsounds/Main002.mp3 temp/".$filename."mixmain2.mp3");
					$mix_str .= " -i temp/".$filename."mixmain2.mp3";
				}
				if ( $mix3 > 0 ) {
					$mixer_count++;
					exec("sox --volume ".$mix3." backsounds/Main003.mp3 temp/".$filename."mixmain3.mp3");
					$mix_str .= " -i temp/".$filename."mixmain3.mp3";
				}
				if ( $mix4 > 0 ) {
					$mixer_count++;
					exec("sox --volume ".$mix4." backsounds/Main004.mp3 temp/".$filename."mixmain4.mp3");
					$mix_str .= " -i temp/".$filename."mixmain4.mp3";
				}
				if ( $mix5 > 0 ) {
					$mixer_count++;
					exec("sox --volume ".$mix5." backsounds/Main005.mp3 temp/".$filename."mixmain5.mp3");
					$mix_str .= " -i temp/".$filename."mixmain5.mp3";
				}
				
				//------- Sound Nine Effect Action -------//		
				
				if ( $_POST['female1'] == 1 ) {
					$mixer_count++;
					$mix_str .= " -i backsounds/Female001.mp3";
				}		
				if ( $_POST['female2'] == 1 ) {
					$mixer_count++;
					$mix_str .= " -i backsounds/Female002.mp3";
				}	
				if ( $_POST['female3'] == 1 ) {
					$mixer_count++;
					$mix_str .= " -i backsounds/Female003.mp3";
				}	
				if ( $_POST['male1'] == 1 ) {
					$mixer_count++;
					$mix_str .= " -i backsounds/Male001.mp3";
				}	
				if ( $_POST['male2'] == 1 ) {
					$mixer_count++;
					$mix_str .= " -i backsounds/Male002.mp3";
				}	
				if ( $_POST['male3'] == 1 ) {
					$mixer_count++;
					$mix_str .= " -i backsounds/Male003.mp3";
				}	
				if ( $_POST['fx1'] == 1 ) {
					$mixer_count++;
					$mix_str .= " -i backsounds/ColdFX001.mp3";
				}	
				if ( $_POST['fx2'] == 1 ) {
					$mixer_count++;
					$mix_str .= " -i backsounds/ColdFX002.mp3";
				}	
				if ( $_POST['fx3'] == 1 ) {
					$mixer_count++;
					$mix_str .= " -i backsounds/ColdFX003.mp3";
				}	
				
				//------------ Mix All Selected Effect Sound ---------------//
					
				
				
				if ( $mix_str == "" ){
					exec("ffmpeg -i temp/".$temp_file." -acodec mp3 -ar 22050 -f wav temp/".$effect_file);
				}else{
					$mixer_count++;
					exec("ffmpeg -i temp/".$temp_file." ".$mix_str." -filter_complex amix=inputs=".$mixer_count.":duration=first temp/".$effect_file); 
				}
				
				if (!unlink("temp/".$temp_file)){return;}
				if (!unlink("temp/".$filename."mixmain1.mp3")){}
				if (!unlink("temp/".$filename."mixmain2.mp3")){}
				if (!unlink("temp/".$filename."mixmain3.mp3")){}
				if (!unlink("temp/".$filename."mixmain4.mp3")){}
				if (!unlink("temp/".$filename."mixmain5.mp3")){}
				
				if (!isset($_SESSION['mix_file'])) {
					$_SESSION['mix_file'] = $effect_file;				
				}
				
				$conf['upload_path'] 	= $this->admin_m->get_upload_path(2,"RecordSounds");
				if (!file_exists($conf['upload_path'])) {
					mkdir($conf['upload_path']);
				}
				
				$nowstamp = date("YmdGis");
				$uname = $this->tank_auth->get_username();
				$temp_file = $uname.$nowstamp.$filename.".mp3";
					
				copy("temp/".$_SESSION['mix_file'], "RecordSounds/".$temp_file);
				if (!unlink("temp/".$_SESSION['mix_file'])){return;}
				unset($_SESSION['mix_file']);
				
				$now = date("Y-m-d G:i:s");
				$this->sounds_m->insert_song($file_name, $temp_file, $now);		
				//echo json_encode($effect_file);
				
			} else {
				return;
			}
			return;
		}
		
	}		
	
	function rec_upload() {
		
		$filename = $_POST['file_name'];
		
		$conf['upload_path'] 	= $this->admin_m->get_upload_path(2,"RecordSounds");
		if (!file_exists($conf['upload_path'])) {
			mkdir($conf['upload_path']);
		}
		
		$nowstamp = date("YmdGis");
		$uname = $this->tank_auth->get_username();
		$stamp = rand();
		$temp_file = $uname.$nowstamp.$stamp.$filename;
			
		copy("temp/".$_SESSION['mix_file'], "RecordSounds/".$temp_file);
		if (!unlink("temp/".$_SESSION['mix_file'])){return;}
		unset($_SESSION['mix_file']);
		
		$now = date("Y-m-d G:i:s");
		$this->sounds_m->insert_song($filename, $temp_file, $now);		
	}
	
	function del_upload() {
		if (!unlink("temp/".$_SESSION['mix_file'])){return;}
		unset($_SESSION['mix_file']);
	}
	
	function vote_act() 
	{
		$songid = $_POST['songid'];
		$voteuserid = $this->tank_auth->get_user_id();
		$wasVote = $this->sounds_m->vote_test($voteuserid, $songid);
		//check if user voted ago then return false
		if ( $wasVote )
		{
			//count vote for songid
			$this->sounds_m->vote_count($songid);
			//insert songid and userid to vote_tbl
			$this->sounds_m->set_vote($songid, $voteuserid);
			echo json_encode("voteOK");
			//return;
		}
		else {
			echo json_encode("voteErr");
			//return;
		}
		
	}
	
}
