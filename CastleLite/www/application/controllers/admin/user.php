<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->ci =& get_instance();
		
		$this->load->helper(array('form', 'url', 'number', 'file'));
		$this->load->library('form_validation');
		$this->load->library('security');
		$this->load->library('tank_auth');
		$this->lang->load('tank_auth');
		$this->load->model('admin_m');
		$this->load->model('sounds_m');
		$this->ci->load->library('session');
		
		$user_idx = $this->tank_auth->get_user_id();
		$role_id = $this->admin_m->get_user_role($user_idx);
		if($role_id!=1) {
			$this->tank_auth->logout();
			redirect('/auth/login');
		}
	}
	
	function _remap($method) {
		$this->load->view('header_v');
		$this->{$method}();
		$this->load->view('footer_v');
	}
	
	function index()
	{
		
	}
	
	function users()
	{		
		$data['rows'] = $this->admin_m->get_user_list();
		$data['post_key'] = "user";
		$this->load->view('admin/user_list_v',$data);
	}
	
	function user_add()
	{
		$data = $this->_proc_user_add();
		$data['post_key'] = "user";
		$this->load->view('admin/user_add_v',$data);
	}
	
	function user_edit()
	{
		$post_id = $this->uri->segment(4, 0);
		if (empty($post_id)) {
			echo "select task!";
			return;
		}
		
		$data = $this->_proc_user_edit($post_id);
		$data['post_key'] = "user";
		$data['post'] = $this->admin_m->get_user_data($post_id);
		$this->load->view('admin/user_edit_v', $data);
	}
	
	function user_del()
	{
		$post_id = $this->uri->segment(4, 0);
		if (empty($post_id)) {
			echo "select task!";
			return;
		}
		$this->_proc_user_del($post_id);
		
		redirect("admin/user/users");
	}
	
	private function &_proc_user_edit($new_idx) {
		
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean');
		$this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean');
		$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
		$this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');
			
		$data = array();
		if ($this->form_validation->run())
		{
			$user_id = $new_idx;
			
			if (!is_null($user = $this->tank_auth->ci->users->get_user_by_id($user_id, TRUE))) {
	
				$old_pass = $this->form_validation->set_value('old_password');
				$new_pass = $this->form_validation->set_value('new_password');
				$new_email = $this->form_validation->set_value('email');
				
				// Check if old password correct
				$hasher = new PasswordHash(
						$this->tank_auth->ci->config->item('phpass_hash_strength', 'tank_auth'),
						$this->tank_auth->ci->config->item('phpass_hash_portable', 'tank_auth'));
				if ($hasher->CheckPassword($old_pass, $user->password)) {			// success
	
					// Hash new password using phpass
					$hashed_password = $hasher->HashPassword($new_pass);
	
					// Replace old password with new one
					$this->tank_auth->ci->users->change_password($user_id, $hashed_password);
					
					// Set new email
					if ($user->email == $new_email) {
						$data['show_message'] = "Auth current email.";
					} elseif ($user->new_email == $new_email) {		// leave email key as is
						$data['new_email_key'] = $user->new_email_key;
						return $data;
	
					} else {// if ($this->tank_auth->ci->users->is_email_available($new_email)) {
						$data['new_email_key'] = md5(rand().microtime());
						$this->tank_auth->ci->users->set_new_email($user_id, $new_email, $data['new_email_key'], TRUE);
						
						$data['show_message'] = "Successfully updated!";	
					} /*else {
						$data['show_message'] = "Auth email in use.";
					}*/
				} else {																						// fail
					$data['show_message'] = "Auth incorrect old password.";
				}
			}
			else {
				$data['show_message'] = "Faild updated!";
			}
		}
		return $data;
	}
	
	private function _proc_song_edit($song_id) {
		
		$this->form_validation->set_rules('song_name', 'Song Name', 'trim|required|xss_clean');
			
		$data = array();
		
		if ($this->form_validation->run())
		{
			
			if (!is_null($song = $this->sounds_m->get_song_by_id($song_id))) {
	
				$new_name = $this->form_validation->set_value('song_name');
	
					// Replace old password with new one
					$this->sounds_m->set_song_name($song_id, $new_name);
					
					$data['show_message'] = "Successfully updated!";
					
			}
			else {
				$data['show_message'] = "Faild updated!";
			}
		}
		return $data;
		
	}
	
	private function _proc_user_del($idx) {
		$strSql = "DELETE FROM users WHERE id='$idx' ";
		$this->db->query($strSql);
	}
	
	private function &_proc_user_add() {
		
		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[confirm_password]|xss_clean');
		$this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'trim|required|xss_clean');
		
		$qry = array();
		$data = array();
		if ($this->form_validation->run())
		{
			
			if (!is_null($data = $this->tank_auth->create_user(
					$this->form_validation->set_value('username'),
					$this->form_validation->set_value('email'),
					$this->form_validation->set_value('password'),
					FALSE))) {
						
				redirect("admin/user/users");							
			}
		}
		return $data;
	}
	
	function songs()
	{
		$data['rows'] = $this->sounds_m->get_song_list();
		$data['post_key'] = "song";
		$this->load->view('admin/song_list_v',$data);
	}
	
	function song_add()
	{
		
	}
	
	function song_edit()
	{
		$post_id = $this->uri->segment(4, 0);
		if (empty($post_id)) {
			echo "select task!";
			return;
		}
		
		$data = $this->_proc_song_edit($post_id);
		$data['post_key'] = "user";
		$data['post'] = $this->sounds_m->get_song_by_id($post_id);
		$this->load->view('admin/song_edit_v', $data);
	}
	
	function song_del()
	{
		$post_id = $this->uri->segment(4, 0);
		if (empty($post_id)) {
			echo "select task!";
			return;
		}
		$this->sounds_m->song_del($post_id);
		redirect("admin/user/songs");
	}
	
	function report_csv() {
		
		$cur_year = $this->uri->segment(5, date('Y'));
		$cur_month = $this->uri->segment(6, date('m'));
		$cur_day = $this->uri->segment(7, date('d'));
				
		$strSql = "SELECT * FROM board order by vote_sum desc ";
		$query = $this->db->query($strSql);
		$rows = $query->result_array();
		if(empty($rows) ) {
			echo "Empty !";
			return;
		}
		$maker_name = $this->ci->session->userdata('username');;
		$file_name = "$maker_name-report-$cur_year-$cur_month-$cur_day.csv";
		
		header("Content-type: text/csv");  
		header("Cache-Control: no-store, no-cache");  
		header('Content-Disposition: attachment; filename="'.$file_name.'"'); 

		$i = 0;
		$header_str = "No, Song Name, File Name, Vote, Created\r\n";
		echo mb_convert_encoding($header_str, "SJIS","UTF-8");
		foreach ($rows as $row) {
			echo 	$row['id'].",".
					$row['song_name'].",".
			  		$row['upload_file'].",".
			  		$row['vote_sum'].",".
			  		$row['created'].","."\r\n";
		}
	  return;
	}
}
/* End of file user.php */
/* Location: ./application/controllers/user.php */