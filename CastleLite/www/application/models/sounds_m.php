<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users
 *
 * This model represents user authentication data. It operates the following tables:
 * - user account data,
 * - user profiles
 *
 * @package	Tank_auth
 * @author	Ilya Konyukhov (http://konyukhov.com/soft/)
 */
class Sounds_m extends CI_Model
{
	private $table_name			= 'board';			// songs table

	
	function __construct()
	{
		parent::__construct();

		$ci =& get_instance();
		$this->table_name = $ci->config->item('db_table_prefix', 'tank_auth').$this->table_name;
		
	}

	/**
	 * Get Song list vote Board
	 * @return list(filename, fileroot)
	 */
	function get_song_list()
	{
		$strSql = "SELECT * FROM board order by vote_sum desc ";
		$query = $this->db->query($strSql);
		$rows = $query->result_array();
		
		return $rows;
		/*
		$query = $this->db->get($this->table_name);
		return $query->row();
		*/
	}
	
	function insert_song($val_str, $fname, $now)
	{
		$strSql ="INSERT INTO board ( song_name, upload_file, created, modified ) VALUES ('".$val_str."', '".$fname."','".$now."','')";
		$query = $this->db->query($strSql);
		return true;
	}
	
	function vote_test($voteuserid, $songid)
	{
		$strSql = "SELECT * FROM vote_tbl WHERE userid='$voteuserid' and songid = '$songid'";
		$query = $this->db->query($strSql);
		$isvote = $query->num_rows();
		if ( $isvote == 1 )
			return false;
		else 	
			return true;
	}
	
	function vote_count($songid)
	{
		$strSql = "SELECT * FROM board WHERE id='$songid'";
		$query = $this->db->query($strSql);
		$row = $query->row_array();
		$count = $row['vote_sum'];
		$count = $count + 1;
		
		$query = $this->db->query("UPDATE board SET vote_sum = '$count' WHERE id='$songid'");
		$query = $this->db->query($strSql);
		return;
	}
	
	function set_vote($songid, $userid)
	{
		$strSql ="INSERT INTO vote_tbl ( userid, songid) VALUES ('".$userid."', '".$songid."')";
		$query = $this->db->query($strSql);
		return true;
	}
	
	function song_del($songid) {
		$strSql = "SELECT * FROM board WHERE id='$songid'";
		$query = $this->db->query($strSql);
		$row = $query->row_array();
		$fname = "songs/".$row['upload_file'];
		$strSql = "DELETE FROM board WHERE id='$songid' ";
		$this->db->query($strSql);
		if( is_file($fname) ) unlink($fname);
	}
	
	function get_song_by_id($songid) {
		$strSql = "SELECT * FROM board WHERE id='$songid'";
		$query = $this->db->query($strSql);
		$row = $query->row_array();
		return $row;
	}
	
	function get_sound_name($songid) {
		$strSql = "SELECT * FROM board WHERE id='$songid'";
		$query = $this->db->query($strSql);
		$row = $query->row_array();
		return $row['song_name'];
	}
		
	function set_song_name($songid,$song_name) {
		$query = $this->db->query("UPDATE board SET song_name = '$song_name'  WHERE id='$songid'");
	}
	
	

}

/* End of file users.php */
/* Location: ./application/models/auth/users.php */