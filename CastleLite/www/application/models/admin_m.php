<?php
class Admin_m extends CI_Model
{
	const USER_TABLE = 'users';
	
	function __construct()
	{
		parent::__construct();
			
	}
	
	function get_user_role($user_idx) {
		$this->db->select('role_id', FALSE);
		$this->db->where('id', $user_idx, FALSE);
	//	echo "<br>sql=".$this->db->_compile_select();
		$query = $this->db->get(self::USER_TABLE);
		$row = $query->row_array();
		if(empty($row)) {
			return 0;
		}
		return $row['role_id'];
	}
	
	function get_user_list() {
		
		$strSql = "SELECT id, username, email, role_id, banned FROM users WHERE 1=1 ";
		$query = $this->db->query($strSql);
		$rows = $query->result_array();
		
		return $rows;
	}	
	
	function  get_user_data($user_idx) {
		
		$strSql = "SELECT id, username, email, password, role_id FROM users WHERE id=".$user_idx;
		$query = $this->db->query($strSql);
		$row = $query->row_array();
		return $row;
	}
	
	function get_next_insert_idx($tbl_name) {

		$next_increment = 0;
		$strSql = "SHOW TABLE STATUS WHERE Name='$tbl_name'";
		$query = $this->db->query($strSql);
		$row = $query->row_array();
		$next_increment = $row['Auto_increment'];
		
		return $next_increment;
	}
	
	function get_song_list() {
		
		$strSql = "SELECT id, song_name, upload_file FROM board WHERE 1=1 ";
		$query = $this->db->query($strSql);
		$rows = $query->result_array();
		
		return $rows;
	}
	
	function get_upload_path($path_type,$media_type='') {
		// local path
		if($path_type==0) {
			return $this->config->item("upload_path")."/".$media_type;
		}
		//http path
		if($path_type==1) { 
			return RT_PATH.substr($this->config->item("upload_path"),1)."/".$media_type;
		}
		//return "http://".$_SERVER['HTTP_HOST'].RT_PATH.$this->config->item("upload_path")."/".$media_type;
		//return "http://".$_SERVER['HTTP_HOST']."/CastleLite/webapp/".$media_type;
		return $media_type;
	}

}