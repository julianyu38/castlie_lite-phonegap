/*
$(document).ready(function() {
	var slider1 = new CircleSlider("rotationSliderContainer");

	slider1.init_Slider( 0 );
});
*/
//$("#create1_pg").live("pageinit",function() {
//});

function CircleSlider(objID)
{
	this.objID = objID;
	this.percent =  0;
	this.radius = $('#' + this.objID).width() / 2 - 5;
	this.srcElem = document.getElementById(this.objID);
    this.srcElem.percent = 0;
    		
	this.init_Slider = function( percent )
	{
		var $container = $('#' + this.objID);
		
		var elem = document.getElementById(this.objID);
		elem.percent = percent;
		canvasWidth = $container.width();
		//radius = canvasWidth / 2;
		
		elem.insertAdjacentHTML("afterBegin","<img class='spin' id='spin_" + this.objID + "' src='images/spin.png' /> <canvas class='myCanvas' style='border:blue solid 0px;' id='myCanvas_" + this.objID + "' width='" + canvasWidth + "' height='" + canvasWidth + "'></canvas>");

		var $spin = $('#spin_' + this.objID );
		var radius = $container.width()/2 - 5;
		var deg = 0;    
		
		X = Math.round(radius* Math.sin(deg*Math.PI/180));
		Y = Math.round(radius*  -Math.cos(deg*Math.PI/180));		
		
		var mdown = false;
		
		$container
		.bind( "vmousedown", function( e ) { mdown = true; e.originalEvent.preventDefault(); })
		.bind( "vmouseup", function( e ) { mdown = false; } )
		.bind( "vmousemove", function( e ) 
//		.vmousedown(function (e) { mdown = true; e.originalEvent.preventDefault(); })
//		.vmouseup(function (e) { mdown = false; })
//		.vmousemove(function (e) 
		{	
			if(mdown)
			{            
				// firefox compatibility
				if(typeof e.offsetX === "undefined" || typeof e.offsetY === "undefined")
				{
					
				   var targetOffset = $(e.target).offset();
				   e.offsetX = (e.pageX - targetOffset.left) * 2;
				   e.offsetY = (e.pageY - targetOffset.top) *2;
				}
				
				if($(e.target).is('#'+ objID))
					var mPos = {x: e.offsetX, y: e.offsetY};
				else
					var mPos = {x: e.target.offsetLeft + e.offsetX, y: e.target.offsetTop + e.offsetY};
					
				var atan = Math.atan2(mPos.x-radius, mPos.y-radius);
				deg = -atan/(Math.PI/180) + 180; // final (0-360 positive) degrees from mouse position            
				
				// for attraction to multiple of 90 degrees
				var distance = Math.abs( deg - ( Math.round(deg / 90) * 90 ) );
				
				if( distance <= 5 )
					deg = Math.round(deg / 90) * 90;
					
				if(deg == 360)
					deg = 0;
				
				if ( deg > 220 ) 
					deg -= 220;	
				else if ( deg < 220 )
					deg = deg + 140;
	
				var roundDeg = Math.round(deg);
				
                elem.percent = Math.round( roundDeg *100 / 270 );
                //this.percent = Math.round( roundDeg *100 / 270 ); 
				//$("#mytext").text("per:" + percent);
	
				$spin.css('-webkit-transform', 'rotate(' + roundDeg + 'deg)');			
				$spin.css('-moz-transform', 'rotate(' + roundDeg + 'deg)');
				$spin.css('-o-transform', 'rotate(' + roundDeg + 'deg)');
				$spin.css('transform', 'rotate(' + roundDeg + 'deg)');
				
				if (roundDeg<=360 && roundDeg <= 270)
				{
					DrawArc(objID, 0, 1, radius, 0.75, roundDeg /180 + 0.75, true);					
					DrawArc(objID, 0, 1, radius, roundDeg /180 + 0.75, 0.25 , false);	
				}
				else
					DrawArc(objID, 0, 1, radius, 0.75, 0.25, false);	
			}//if(mdown)
		});
		
		// draw arc when load the page
		DrawArc(objID,0, 1, this.radius, 0.75,  percent * ( Math.PI - 0.47 )/ 180 +0.25, true);
		DrawArc(objID,0, 1, this.radius, percent * ( Math.PI - 0.47 ) / 180  +0.75,  0.25, false);
		
		roundDeg = percent * ( Math.PI - 0.47 );
		$spin.css('-webkit-transform', 'rotate(' + roundDeg + 'deg)');			
		$spin.css('-moz-transform', 'rotate(' + roundDeg + 'deg)');
		$spin.css('-o-transform', 'rotate(' + roundDeg + 'deg)');
		$spin.css('transform', 'rotate(' + roundDeg + 'deg)');

	};

	this.DrawArcs = function( st_alpha, dt_alpha, r, s_a, e_a, isRedBar)
	{
		DrawArc(objID,st_alpha, dt_alpha, r, s_a, e_a, isRedBar);
	};
	
	this.GetPercent = function()
	{
		return this.srcElem.percent;
	};

}

function DrawArc(id, st_alpha, dt_alpha, r, s_a, e_a, isRedBar)
{
	var c=document.getElementById("myCanvas_"+id);
	var ctx=c.getContext("2d");
	
	ctx.beginPath();
	
	var st_x = c.width / 2;
	var st_y = c.width / 2;
	
	if ( s_a < 0.75 )
		s_a = 0.75;

	if ( e_a > 0.25 )
		e_a = 0.25;
	
	var st_a = Math.PI*s_a;
	var ed_a = Math.PI*e_a;
	
	var ccw=false;
	
	ctx.arc( st_x,st_y,r,st_a,ed_a,ccw);		
	
	if (isRedBar == true)
		ctx.strokeStyle = "#ff0000";
	else	
		ctx.strokeStyle = "#585858";
		
	ctx.lineWidth = 10;
	ctx.stroke();
	ctx.closePath();
};