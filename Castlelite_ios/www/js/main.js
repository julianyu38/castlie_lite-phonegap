var g_server = "http://175.160.103.10/CastleLite/www/";
var g_song_url = g_server + "RecordSounds";
var g_server_url = g_server + "index.php/api/";
var g_uname = "";
var g_rec_song = "";
var g_file_name="";


var mix1 = 0;
var mix2 = 0;
var mix3 = 0;
var mix4 = 0;
var mix5 = 0;

var mix_female1 = 0;
var mix_female2 = 0;
var mix_female3 = 0;
var mix_male1 = 0;
var mix_male2 = 0;
var mix_male3 = 0;
var mix_fx1 = 0;
var mix_fx2 = 0;
var mix_fx3 = 0;  

var slider1;
var slider2;
var slider3;
var slider4;
var slider5;

var voice_rec = null;
var back_female1 = null;
var back_female2 = null;
var back_female3 = null;
var back_male1 = null;
var back_male2 = null;
var back_male3 = null;
var back_fx1 = null;
var back_fx2 = null;
var back_fx3 = null;
var main1 = null;
var main2 = null;
var main3 = null;
var main4 = null;
var main5 = null;

var rec = null;

$(document).ready(function() {
    
    App.init();     
    
    $('#checkbox-1').click(function(){
        if ( $('#checkbox-1').attr("checked") == "checked" ){
            mix_female1 = 1;
            back_female1 = new Media(g_server+"backsounds/Female001.mp3",
                                   function(){console.log("back_female1 success!");},
                                   function(){console.log("back_female1 failed!");});
            back_female1.play({ numberOfLoops:1000});
        }else{
            mix_female1 = 0;
            back_female1.stop();
        }
    });
    $('#checkbox-2').click(function(){
        if ( $('#checkbox-2').attr("checked") == "checked" ){
            mix_female2 = 1;
            back_female2 = new Media(g_server+"backsounds/Female002.mp3",
                                     function(){console.log("back_female2 success!");},
                                     function(){console.log("back_female2 failed!");});
            back_female2.play({ numberOfLoops:1000});
        }else{
            mix_female2 = 0;
            back_female2.stop();
        }
    });
    $('#checkbox-3').click(function(){
        if ( $('#checkbox-3').attr("checked") == "checked" ){
            mix_female3 = 1;
            back_female3 = new Media(g_server+"backsounds/Female003.mp3",
                                     function(){console.log("back_female3 success!");},
                                     function(){console.log("back_female3 failed!");});
            back_female3.play({ numberOfLoops:1000});
        }else{
            mix_female3 = 0;
            back_female3.stop();
        }
    });
    $('#checkbox-4').click(function(){
        if ( $('#checkbox-4').attr("checked") == "checked" ){
            mix_male1 = 1;
            back_male1 = new Media(g_server+"backsounds/Male001.mp3",
                                     function(){console.log("back_male1 success!");},
                                     function(){console.log("back_male1 failed!");});
            back_male1.play({ numberOfLoops:1000});
        }else{
            mix_male1 = 0;
            back_male1.stop();
        }
    });
    $('#checkbox-5').click(function(){
        if ( $('#checkbox-5').attr("checked") == "checked" ){
            mix_male2 = 1;
            back_male2 = new Media(g_server+"backsounds/Male002.mp3",
                                     function(){console.log("back_male2 success!");},
                                     function(){console.log("back_male2 failed!");});
            back_male2.play({ numberOfLoops:1000});
        }else{
            mix_male2 = 0;
            back_male2.stop();
        }
    });
    $('#checkbox-6').click(function(){
        if ( $('#checkbox-6').attr("checked") == "checked" ){
            mix_male3 = 1;
            back_male3 = new Media(g_server+"backsounds/Male003.mp3",
                                     function(){console.log("back_male3 success!");},
                                     function(){console.log("back_male3 failed!");});
            back_male3.play({ numberOfLoops:1000});
        }else{
            mix_male3 = 0;
            back_male3.stop();
        }
    });
    $('#checkbox-7').click(function(){
        if ( $('#checkbox-7').attr("checked") == "checked" ){
            mix_fx1 = 1;
            back_fx1 = new Media(g_server+"backsounds/ColdFX001.mp3",
                                     function(){console.log("back_fx1 success!");},
                                     function(){console.log("back_fx1 failed!");});
            back_fx1.play({ numberOfLoops:1000});
        }else{
            mix_fx1 = 0;
            back_fx1.stop();
        }
    });
    $('#checkbox-8').click(function(){
        if ( $('#checkbox-8').attr("checked") == "checked" ){
            mix_fx2 = 1;
            back_fx2 = new Media(g_server+"backsounds/ColdFX002.mp3",
                                     function(){console.log("back_fx2 success!");},
                                     function(){console.log("back_fx2 failed!");});
            back_fx2.play({ numberOfLoops:1000});
        }else{
            mix_fx2 = 0;
            back_fx2.stop();
        }
    });
    $('#checkbox-9').click(function(){
        if ( $('#checkbox-9').attr("checked") == "checked" ){
            mix_fx3 = 1;
            back_fx3 = new Media(g_server+"backsounds/ColdFX003.mp3",
                                     function(){console.log("back_fx3 success!");},
                                     function(){console.log("back_fx3 failed!");});
            back_fx3.play({ numberOfLoops:1000});
        }else{
            mix_fx3 = 0;
            back_fx3.stop();
        }
    });
           
    $('#checkbox-01').click(function(){
        if ( $('#checkbox-01').attr("checked") == "checked" ){
            mix1 = 1;
            main1 = new Media(g_server+"backsounds/Main001.mp3",
                                   function(){console.log("back_female1 success!");},
                                   function(){console.log("back_female1 failed!");});
            main1.play({ numberOfLoops:1000});
        }else{
            mix1 = 0;
            main1.stop();
        }
    });
    $('#checkbox-02').click(function(){
        if ( $('#checkbox-02').attr("checked") == "checked" ){
            mix2 = 1;
            main2 = new Media(g_server+"backsounds/Main002.mp3",
                                     function(){console.log("back_female2 success!");},
                                     function(){console.log("back_female2 failed!");});
            main2.play({ numberOfLoops:1000});
        }else{
            mix2 = 0;
            main2.stop();
        }
    });
    $('#checkbox-03').click(function(){
        if ( $('#checkbox-03').attr("checked") == "checked" ){
            mix3 = 1;
            main3 = new Media(g_server+"backsounds/Main003.mp3",
                                     function(){console.log("back_female3 success!");},
                                     function(){console.log("back_female3 failed!");});
            main3.play({ numberOfLoops:1000});
        }else{
            mix3 = 0;
            main3.stop();
        }
    });
    $('#checkbox-04').click(function(){
        if ( $('#checkbox-04').attr("checked") == "checked" ){
            mix4 = 1;
            main4 = new Media(g_server+"backsounds/Main004.mp3",
                                     function(){console.log("back_male1 success!");},
                                     function(){console.log("back_male1 failed!");});
            main4.play({ numberOfLoops:1000});
        }else{
            mix4 = 0;
            main4.stop();
        }
    });
    $('#checkbox-05').click(function(){
        if ( $('#checkbox-05').attr("checked") == "checked" ){
            mix5 = 1;
            main5 = new Media(g_server+"backsounds/Main005.mp3",
                                     function(){console.log("back_male2 success!");},
                                     function(){console.log("back_male2 failed!");});
            main5.play({ numberOfLoops:1000});
        }else{
            mix5 = 0;
            main5.stop();
        }
    });
    
    
});

App = {
	init : function() {
        
		var _self = this;
        
		_self.doResize();
        
		$(window).bind('resize', function() {
			_self.doResize();
		});
        
        $(window).bind('orientationchange', function() {
			_self.doResize();
		});
        
		$('#vote_pg').live('pageshow', function() {
            $.mobile.loading( 'show');            
			get_json(	
            g_server_url + "getBoardData", 
            "POST", 
            "", 
            function(data) {
                
                html = "";
                $.each(data, function(i) {
                    var songid = data[i].id;
                    var songname = data[i].song_name;
                    var songfile = data[i].upload_file;		
                    var no = i + 1;
                    html += "<li style='padding-bottom:50px;padding-top:50px'>";
                    html += "<a id='" + songid + "'><label class = 'songlist'>" + no + ":</label><label style='font-size:120px;width:1200px;'>"+songname+"</label><input type='hidden' value='" + songfile + "'/><input type='hidden' value='" + songname + "'/></a>";
                    html += "<a href='#playdiv' onclick='play_song(" + songid + ");' data-rel='popup' data-position-to='window' data-inline='true'  data-textonly='false' data-textvisible='true' data-msgtext ='Please wait...'>";
                    html += "<img src='images/play_btn.png' class='play_btn' /></a>";
                    html += "<a href='' onclick='vote_song(" + songid + ");' data-rel='' data-position-to='window' data-inline='true' data-textonly='false' data-textvisible='true' data-msgtext ='Please wait...'>";
                    html += "<img src='images/vote_btn.png' class='play_btn' /></a>";
                    html += "</li>";
                    
                });
                $('.scroller').html("");
                $('.scroller').html(html);
                $.mobile.loading('hide');
            }
			);
            
                           back_female1.stop();
                           back_female2.stop();
                           back_female3.stop();
                           
                           back_male1.stop();
                           back_male2.stop();
                           back_male3.stop();
                           
                           back_fx1.stop();
                           back_fx2.stop();
                           back_fx3.stop();
                           
                           main1.stop();
                           main2.stop();
                           main3.stop();
                           main4.stop();
                           main5.stop();
                           
		});
        
        $("#create1_pg").live("pageinit",function() {
            
            slider1 = new CircleSlider("rotationSliderContainer1");
            slider1.init_Slider( 0 );
            slider2 = new CircleSlider("rotationSliderContainer2");
            slider2.init_Slider( 0 );
            slider3 = new CircleSlider("rotationSliderContainer3");
            slider3.init_Slider( 0 );
            slider4 = new CircleSlider("rotationSliderContainer4");
            slider4.init_Slider( 0 );
            slider5 = new CircleSlider("rotationSliderContainer5");
            slider5.init_Slider( 0 );                        
        });
        
        

	},
	/**------------ For Control display size -----------------**/
	doResize : function() {
		var minRatio = 0.1;
		if(!this.boundObj)
        this.boundObj = $('.screen-wrapper');
		var ratioW = ($(window).width()) / this.boundObj.width();
		/*var ratioW;
        	width = $(window).height() / this.boundObj.height() *  this.boundObj.width();
			ratioW = width / this.boundObj.width();*/
		var ratioH = ($(window).height()) / this.boundObj.height();
		var ratio = ratioH < ratioW ? ratioH : ratioW;
		ratio = ratio > minRatio ? ratio : minRatio;
		this.boundObj.css('-webkit-transform', 'scale(' + ratio + ', ' + ratio + ')');
		this.boundObj.css('-webkit-transform-origin', '0% 0%');
		this.boundObj.css('-moz-transform', 'scale(' + ratio + ', ' + ratio + ')');
		this.boundObj.css('-moz-transform-origin', '0% 0%');
		this.boundObj.css('-ms-transform', 'scale(' + ratio + ', ' + ratio + ')');
		this.boundObj.css('-ms-transform-origin', '0% 0%');
	}
}

//--- get json data ---//
function get_json(_url, _type, _data, _func) {
	console.log("========= Server URL : " + _url);
	$.ajax({
		url : _url,
		type : _type,
		//	contentType: "application/json",
		dataType : 'json',
		data : _data,
		success : _func,
		error : function(jqXHR, ajaxOptions, thrownError) {
            $.mobile.loading('hide');
			console.log("Getting Json Dat Error : ======");
			console.log(jqXHR);
			if(jqXHR.status === 0) {
				alert('Not connect.\n Verify Network.');
			} else if(jqXHR.status == 404) {
				alert('Requested page not found. [404]');
			} else if(jqXHR.status == 500) {
				alert('Internal Server Error [500].');
			} else if(thrownError === 'parsererror') {
				alert('Requested JSON parse failed.');
			} else if(thrownError === 'timeout') {
				alert('Time out error.');
			} else if(thrownError === 'abort') {
				alert('Ajax request aborted.');
			} else {
				alert('Uncaught Error.\n' + jqXHR.responseText);
			}
		}
	});
}

//--- Login Button Action ---//
$("#login_pg #login_btn").live("click", function() {
    $.mobile.loading( 'show');
    get_json(
    g_server_url + "login", 
    "POST", 
    {
        "uname" : $("#login_name").val(),
        "upass" : $("#login_pass").val()
    }, 
    function(data) {
        $.mobile.loading('hide');
        if(data == "LoginOk") {
            g_uname = $("#login_name").val();
            $.mobile.changePage("#main_pg", {transition : "none"});
            console.log("---Login OK---");
        } else {
            console.log("~~~ Login Err~~~");
            alert("Login Failed!");
        }
    }
	);
});

//--- Logout Button Action ---//
$("#vote_pg #logout_btn").live("click", function() {
	g_uname = "";
	$.mobile.changePage("#home_pg", { transition : "none" });
});

//--- User Regist Button Action ---//
$("#register_pg #reg_btn").live("click", function() {
    $.mobile.loading( 'show');
	get_json(
    g_server_url + "register", 
    "POST", 
    {
        "uname" : $("#reg_name").val(),
        "umail" : $("#reg_email").val(),
        "upass" : $("#reg_pass").val()
    }, 
    function(data) {
        $.mobile.loading('hide');
        if(data == "RegOK") {
            console.log("===RegOK!===");
            g_uname = $("#reg_name").val();
            $.mobile.changePage("#main_pg", { transition : "none" });
        } else if(data == "RegErr") {
            console.log("===RegErr!===");
            alert("Regist Failed!");
        } else {
            alert("Regist Invalid!");
        }
    }
	);
});
// --- For Audio Recording --- //
$("#create2_pg #rec_btn").live("click", function() {
    
    /*var rec_timer = null;
    if (mediaTimer == null) {
        mediaTimer = setInterval(function() {
            background_play();                                            
        }, 1000);
    }*/
    navigator.device.capture.captureAudio(captureSuccess, captureError, {limit : 1});
    
});

function stop_rec(){
    //rec.stopRecord();
}

// Called when capture operation is finished
function captureSuccess(mediaFiles) {
	console.log("Capture Sound Success!");
    var i, len;
	len = mediaFiles.length;
	for( i = 0; i < len; i++) {
		$('.savesongitem').attr("style", "display:show");
		g_rec_song = mediaFiles[i];
    }    
    medianame = g_rec_song.name;
	namesplite = medianame.split(".");
    $('#songtype').html("Input new name for " + medianame );
    $('#savesongname').val(namesplite[0]);
    $('#rec_play_song').html("<embed style='margin:5%;width:90%' src='"+g_rec_song.fullPath+ "'></embed");
    $.mobile.changePage("#create2_pg", { transition : "none" });
}

// Called if something bad happens.
function captureError(error) {
    console.log("Capture Sound Error!");
	//var msg = 'An error occurred during capture: ' + error.code;
	//navigator.notification.alert(msg, null, 'Uh oh!');
}

function play_Rec() {
    $('#rec_play_btn').attr("data-theme", "b");
    voice_rec = new Media(g_rec_song.fullPath,
                                     function(){console.log("voice rec play success!");},
                                     function(){console.log("voice rec play failed!");});
    voice_rec.play();
}

function stop_play_Rec() {
    voice_rec.play();
}

function merge_sounds() {
    
    $.mobile.loading( 'show');
    
    var recFile = g_rec_song;
    medianame = recFile.name;
	namesplite = medianame.split(".");
    
    var options = new FileUploadOptions();    
    options.fileKey = "file";
    //options.fileName = medianame;
    options.fileName = $('#savesongname').val();
    options.mimeType = "audio/wav"; 
    
    var params = new Object();
    
    var rnd = Math.random();
    var date = new Date();
    
    g_file_name = Math.floor(rnd*Math.floor(date.getFullYear())*Math.floor(date.getMonth())*Math.floor(date.getDate())*Math.floor(date.getHours())*Math.floor(date.getMinutes())*Math.floor(date.getSeconds())*Math.floor(date.getMilliseconds())*10000000);
    params.fname = g_file_name;
    params.fext = namesplite[1];
    
    params.mix1 = slider1.GetPercent();
    params.mix2 = slider2.GetPercent();
    params.mix3 = slider3.GetPercent();
    params.mix4 = slider4.GetPercent();
    params.mix5 = slider5.GetPercent();
    params.female1 = mix_female1;
    params.female2 = mix_female2;
    params.female3 = mix_female3;
    params.male1 = mix_male1;
    params.male2 = mix_male2;
    params.male3 = mix_male3;
    params.fx1 = mix_fx1;
    params.fx2 = mix_fx2;
    params.fx3 = mix_fx3;
    options.params = params;
    
    var ft = new FileTransfer();     

    ft.upload(recFile.fullPath, 
            g_server_url + "rec_merge", 
            function(result) {   
                $.mobile.loading('hide'); 
                alert("Record Saved Success!");              
            }, function(error) {
                $.mobile.loading('hide');
                $('.savesongitem').attr("style", "display:none");
                $('#songtype').html("There is no file for save.");
                $('#rec_play_song').html("");
                alert("Record Failed!");
                console.log('Error record merge file ' + path + ': ' + error.code);
            }, options);  
            
}
    
//Save and Upload files to server
function uploadFile() {
    $.mobile.loading('show');
    if ( trim($('#savesongname').val()) == "" ){  
        
        alert("Please insert file name!");
    
    }else{  
        
        merge_sounds();
        
        $.mobile.changePage("#create2_pg", { transition : "none" });
        
    }
}

function deleteTemp() {
    $.mobile.loading('show');
    get_json(
    g_server_url + "del_upload", 
    "POST", 
    {
        "file_name" : g_file_name
    }, 
    function(data) {
        $.mobile.loading('hide');
        alert( fileName + " is cancelled successfully!");
    }
    );

}

// --- For Play Song popup --- //
function play_song(songid) {
    $.mobile.loading('hide');
	fname = $('#' + songid + " input:nth-child(odd)").val();
	name = $('#' + songid + " input:nth-child(even)").val();
	src = g_song_url + "/" + fname;   
    $('#playsongname').html("<label style='font-size:30px'>"+name+"</label>");
	$('#playsong').html("<embed style='width:100%;'  src='" + src + "' ></embed><br/>");
}

// --- For Vote Song  --- //
function vote_song(songid) {
	get_json(
    g_server_url + "vote_act", 
    "POST", 
    {
        "songid" : songid
    }, 
    function(data) {
        $.mobile.loading('hide');
        if(data == "voteOK") {
            console.log("===voteOK!===");
            //$('#votesong').html("<label style='font-size:30px;'>You are voted successfully!</label>");
            alert("You are voted successfully!");
        } else if(data == "voteErr") {
            console.log("=== Vote Already!===");
            //$('#votesong').html("<label style='font-size:30px;'>You were voted already!</label>");
            alert("You were voted already!");
        } else {
            //$('#votesong').html("<label style='font-size:30px;padding:20px;'>Invalid!</label>");
            alert("Invalid!");
        }
    }
	);
}


function trim(string){ 
    return string.toString().replace(/^\s+/, "").replace(/\s+$/, ""); 
}

$(document).on("click", ".show-page-loading-msg", function() {
    var $this = $( this ),
    theme = $this.jqmData("theme") || $.mobile.loader.prototype.options.theme,
    msgText = $this.jqmData("msgtext") || $.mobile.loader.prototype.options.text,
    textVisible = $this.jqmData("textvisible") || $.mobile.loader.prototype.options.textVisible,
    textonly = !!$this.jqmData("textonly");
    html = $this.jqmData("html") || "";
    $.mobile.loading( 'show', {
        text: msgText,
        textVisible: textVisible,
        theme: theme,
        textonly: textonly,
        html: html
    });
})
